// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

// NOTE: MRTK Shaders are versioned via the MRTK.Shaders.sentinel file.
// When making changes to any shader's source file, the value in the sentinel _must_ be incremented.

///
/// Basic wireframe shader that can be used for rendering spatial mapping meshes.
///
Shader "Custom/CustomWireframeShader"
{
    Properties
    {
        // Advanced options.
        [Enum(RenderingMode)] _Mode("Rendering Mode", Float) = 0                                     // "Opaque"
        [Enum(CustomRenderingMode)] _CustomMode("Mode", Float) = 0                                   // "Opaque"
        [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend("Source Blend", Float) = 1                 // "One"
        [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend("Destination Blend", Float) = 0            // "Zero"
        [Enum(UnityEngine.Rendering.BlendOp)] _BlendOp("Blend Operation", Float) = 0                 // "Add"
        [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("Depth Test", Float) = 4                // "LessEqual"
        [Enum(DepthWrite)] _ZWrite("Depth Write", Float) = 1                                         // "On"
        _ZOffsetFactor("Depth Offset Factor", Float) = 0                                             // "Zero"
        _ZOffsetUnits("Depth Offset Units", Float) = 0                                               // "Zero"
        [Enum(UnityEngine.Rendering.ColorWriteMask)] _ColorWriteMask("Color Write Mask", Float) = 15 // "All"
        [Enum(UnityEngine.Rendering.CullMode)] _CullMode("Cull Mode", Float) = 2                     // "Back"
        _RenderQueueOverride("Render Queue Override", Float) = -1

        _BaseColor("Base color", Color) = (0.0, 0.0, 0.0, 1.0)
        _WireColorHigh("Wire color high", Color) = (1.0, 0.0, 0.0, 1.0)
        _WireColorMid("Wire color mid", Color) = (0.0, 1.0, 0.0, 1.0)
        _WireColorLow("Wire color low", Color) = (0.0, 0.0, 1.0, 1.0)
        _DepthPerLayer("Depth per layer", float) = 50.0
        _WireThickness("Wire thickness", float) = 100.0
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        Blend[_SrcBlend][_DstBlend]
        BlendOp[_BlendOp]
        ZTest[_ZTest]
        ZWrite[_ZWrite]
        Cull[_CullMode]
        Offset[_ZOffsetFactor],[_ZOffsetUnits]
        ColorMask[_ColorWriteMask]

        Pass
        {
            Offset 50, 100

            CGPROGRAM
            #pragma vertex vert
            #pragma geometry geom
            #pragma fragment frag

            #if defined(SHADER_API_D3D11)
            #pragma target 5.0
            #endif

            #include "UnityCG.cginc"

            float4 _BaseColor;
            float4 _WireColorHigh;
            float4 _WireColorMid;
            float4 _WireColorLow;
            float _DepthPerLayer;
            float _WireThickness;

            // Based on approach described in Shader-Based Wireframe Drawing (2008)
            // http://orbit.dtu.dk/en/publications/id(13e2122d-bec7-48de-beca-03ce6ea1c3f1).html

            struct v2g
            {
                float4 viewPos : SV_POSITION;
                float3 worldPos : TEXCOORD0;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            v2g vert(appdata_base v)
            {
                UNITY_SETUP_INSTANCE_ID(v);
                v2g o;
                o.viewPos = UnityObjectToClipPos(v.vertex);
                o.worldPos = v.vertex.xyz;
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                return o;
            }

            // inverseW is to counteract the effect of perspective-correct interpolation so that the lines
            // look the same thickness regardless of their depth in the scene.
            struct g2f
            {
                float4 viewPos : SV_POSITION;
                float3 worldPos : TEXCOORD0;
                float inverseW : TEXCOORD1;
                float3 dist : TEXCOORD2;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            [maxvertexcount(3)]
            void geom(triangle v2g i[3], inout TriangleStream<g2f> triStream)
            {
                // Calculate the vectors that define the triangle from the input points.
                float2 point0 = i[0].viewPos.xy / i[0].viewPos.w;
                float2 point1 = i[1].viewPos.xy / i[1].viewPos.w;
                float2 point2 = i[2].viewPos.xy / i[2].viewPos.w;

                // Calculate the area of the triangle.
                float2 vector0 = point2 - point1;
                float2 vector1 = point2 - point0;
                float2 vector2 = point1 - point0;
                float area = abs(vector1.x * vector2.y - vector1.y * vector2.x);

                float3 distScale[3];
                distScale[0] = float3(area / length(vector0), 0, 0);
                distScale[1] = float3(0, area / length(vector1), 0);
                distScale[2] = float3(0, 0, area / length(vector2));

                float wireScale = 800 - _WireThickness;

                // Output each original vertex with its distance to the opposing line defined
                // by the other two vertices.
                g2f o;
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                [unroll]
                for (uint idx = 0; idx < 3; ++idx)
                {
                   o.viewPos = i[idx].viewPos;
                   o.worldPos = i[idx].worldPos;
                   o.inverseW = 1.0 / o.viewPos.w;
                   o.dist = distScale[idx] * o.viewPos.w * wireScale;
                   UNITY_TRANSFER_VERTEX_OUTPUT_STEREO(i[idx], o);
                   triStream.Append(o);
                }
            }

            float4 RGBToHSV(float4 rgb)
            {
                float r = rgb.r;
                float g = rgb.g;
                float b = rgb.b;

                float minComponent = min(min(r, g), b);
                float maxComponent = max(max(r, g), b);
                float delta = maxComponent - minComponent;

                float h = 0;
                float s = 0;
                float v = maxComponent;

                if (delta > 0.0)
                {
                    if (maxComponent == r)
                    {
                        h = (g - b) / delta;
                        if (h < 0) h += 6;
                    }
                    else if (maxComponent == g)
                    {
                        h = 2 + (b - r) / delta;
                    }
                    else
                    {
                        h = 4 + (r - g) / delta;
                    }

                    h /= 6;
                    s = delta / maxComponent;
                }

                return float4(h, s, v, rgb.a);
            }

            float4 HSVToRGB(float h, float s, float v)
            {
                float4 rgb;

                if (s == 0)
                {
                    rgb = float4(v, v, v, 1);
                }
                else
                {
                    h *= 6;
                    int i = floor(h);
                    float f = h - i;
                    float p = v * (1 - s);
                    float q = v * (1 - s * f);
                    float t = v * (1 - s * (1 - f));

                    if (i == 0) rgb = float4(v, t, p, 1);
                    else if (i == 1) rgb = float4(q, v, p, 1);
                    else if (i == 2) rgb = float4(p, v, t, 1);
                    else if (i == 3) rgb = float4(p, q, v, 1);
                    else if (i == 4) rgb = float4(t, p, v, 1);
                    else rgb = float4(v, p, q, 1);
                }

                return rgb;
            }

            float4 frag(g2f i) : COLOR
            {
                // Calculate  minimum distance to one of the triangle lines, making sure to correct
                // for perspective-correct interpolation.
                float dist = min(i.dist[0], min(i.dist[1], i.dist[2])) * i.inverseW;

                float y = -i.worldPos.y / _DepthPerLayer;

                if (y > 1.0)
                {
                    y = 1.0;
                }
                else if (y < 0) {
                    y = 0.0;
                }

                float4 hsvColor1;
                float4 hsvColor2;

                if (y < 0.5) 
                {
                    y = y * 2;

                    hsvColor1 = RGBToHSV(_WireColorHigh);
                    hsvColor2 = RGBToHSV(_WireColorMid);
                }
                else
                {
                    y = (y - 0.5) * 2;

                    hsvColor1 = RGBToHSV(_WireColorMid);
                    hsvColor2 = RGBToHSV(_WireColorLow);
                }

                float lerpedHue = lerp(hsvColor1.x, hsvColor2.x, y);
                float lerpedSaturation = lerp(hsvColor1.y, hsvColor2.y, y);
                float lerpedValue = lerp(hsvColor1.z, hsvColor2.z, y);
                
                // Convert back to RGB
                float4 wireColor = HSVToRGB(lerpedHue, lerpedSaturation, lerpedValue);

                // Make the intensity of the line very bright along the triangle edges but fall-off very
                // quickly.
                float I = exp2(-2 * dist * dist);

                float3 color = I * wireColor.rgb + (1 - I) * _BaseColor.rgb;

                return float4(color, 1);
            }
            ENDCG
        }
    }

    FallBack "Mixed Reality Toolkit/Standard"
}

using UnityEngine;

public class UserMovement : MonoBehaviour
{
    [SerializeField]
    private float Knots = 60f;

    [SerializeField]
    private Transform Camera;

    private const float KNOT_TO_MPS = 0.5144f;

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 cameraForward = Camera.forward;
        cameraForward.y = 0f;
        cameraForward.Normalize();

        Vector3 cameraRight = Camera.right;
        cameraRight.y = 0f;
        cameraRight.Normalize();

        Vector3 moveDirection = (cameraForward * vertical + cameraRight * horizontal).normalized;
        transform.position += KNOT_TO_MPS * Knots * Time.fixedDeltaTime * moveDirection;
    }
}

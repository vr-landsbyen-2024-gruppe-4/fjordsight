﻿using UnityEngine;

public struct DepthPoint
{
    public Vector2 Position;
    public float Depth;
    public DepthType DepthType;
}
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static IEnumerable<Vector2Int> GetPerimeter(Vector2Int start, Vector2Int end, int stepSize = 1)
    {
        // Left side
        for (int y = start.y; y <= end.y; y += stepSize)
        {
            yield return new Vector2Int(start.x, y);
        }

        // Top side
        for (int x = start.x + stepSize; x <= end.x; x += stepSize)
        {
            yield return new Vector2Int(x, end.y);
        }

        // Right side
        for (int y = end.y - stepSize; y >= start.y; y -= stepSize)
        {
            yield return new Vector2Int(end.x, y);
        }

        // Bottom side
        for (int x = end.x - stepSize; x > start.x; x -= stepSize)
        {
            yield return new Vector2Int(x, start.y);
        }
    }
}
﻿using TMPro;
using UnityEngine;

public class Ship : MonoBehaviour
{
    private const float KNOT_TO_MPS = 0.5144f;
    private const float MIN_PER_SEC = 1f / 60f;

    [SerializeField]
    private float FontSize =5f;

    [SerializeField]
    private float LerpAmount = 0.01f;

    [SerializeField]
    private Transform _model;

    [SerializeField]
    private TextMeshProUGUI _text;

    private ShipData _data;
    private bool _initialized = false;

    private Vector3 _truePosition = new();

    public void SetShipData(ShipData data)
    {
        _data = data;

        _model.localPosition = data.Offset;
        _model.localScale = data.Size;

        _text.text = data.Name;
        _text.fontSize = FontSize;
        _text.rectTransform.sizeDelta = new(_text.rectTransform.sizeDelta.x, FontSize);

        var canvasRect = _text.canvas.GetComponent<RectTransform>();
        canvasRect.sizeDelta = _text.rectTransform.sizeDelta;
        canvasRect.localPosition = new(0, (FontSize + data.Size.y) / 2 + 1, 0);

        _truePosition = data.Position;

        transform.rotation = Quaternion.identity;
        transform.Rotate(Vector3.up, data.Rotation);

        if (!_initialized)
        {
            transform.position = _truePosition;
            _initialized = true;
        }
    }

    private void Update()
    {
        var delta = KNOT_TO_MPS * Time.deltaTime * _data.Speed * _data.Direction;

        _truePosition += delta;
        transform.position += delta;

        transform.position = Vector3.Lerp(transform.position, _truePosition, LerpAmount);

        transform.Rotate(Vector3.up, MIN_PER_SEC * Time.deltaTime * _data.AngularSpeed);
    }
}

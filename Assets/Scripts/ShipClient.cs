using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using UnityEngine;

public class ShipClient
{
    private FormUrlEncodedContent _tokenRequest = new(new Dictionary<string, string>
    {
        { "client_id", "vetlems@stud.ntnu.no:FjordSight" },
        { "client_secret", "FjordSightFjordSight" },
        { "scope", "ais" },
        { "grant_type", "client_credentials" }
    });

    private DateTime _expires;
    private HttpClient _tokenClient;
    private HttpClient _client;

    public ShipClient()
    {
        _tokenClient = new()
        {
            BaseAddress = new("https://id.barentswatch.no/connect/token")
        };
        _client = new()
        {
            BaseAddress = new("https://live.ais.barentswatch.no/live/v1/latest/combined"),
        };
        _client.DefaultRequestHeaders.Accept.Add(new("application/json"));
    }

    public async Task<List<ShipResponse>> GetShipsAsync(Vector2 bottomLeft, Vector2 topRight)
    {
        if (DateTime.UtcNow >= _expires)
        {
            await ConnectAsync();
        }

        var coordinates = new double[][]
        {
            new double[] {
                bottomLeft.x,
                topRight.y
            },
            new double[] {
                bottomLeft.x,
                bottomLeft.y
            },
            new double[] {
                topRight.x,
                bottomLeft.y
            },
            new double[] {
                topRight.x,
                topRight.y
            },
            new double[] {
                bottomLeft.x,
                topRight.y
            }
        };

        var shipRequest = new ShipRequest(coordinates);
        var shipRequestString = JsonSerializer.SerializeToElement(shipRequest).GetRawText();

        var requestContent = new StringContent(shipRequestString, Encoding.UTF8, "application/json");
        var response = await _client.PostAsync("", requestContent);

        var responseStream = await response.Content.ReadAsStreamAsync();
        return await JsonSerializer.DeserializeAsync<List<ShipResponse>>(responseStream);
    }

    private async Task ConnectAsync()
    {
        var response = await _tokenClient.PostAsync("", _tokenRequest);
        var responseStream = await response.Content.ReadAsStreamAsync();
        var tokenResponse = await JsonSerializer.DeserializeAsync<TokenResponse>(responseStream);
        _client.DefaultRequestHeaders.Authorization = new("Bearer", tokenResponse.access_token);
        _expires = DateTime.UtcNow.AddSeconds(tokenResponse.expires_in - 60);
    }
}
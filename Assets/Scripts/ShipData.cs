﻿using UnityEngine;

public struct ShipData
{
    public string Name;
    public Vector3 Size;
    public Vector3 Position;
    public float Rotation;
    public Vector3 Offset;
    public float Speed;
    public Vector3 Direction;
    public float AngularSpeed;
}
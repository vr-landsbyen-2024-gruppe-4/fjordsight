﻿using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    private void Update()
    {
        transform.LookAt(Camera.main.transform.position);
        transform.Rotate(Vector3.up, 180);
    }
}
﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using Unity.VectorGraphics;
using UnityEngine;

public class InfoPointGenerator : MonoBehaviour
{
    [SerializeField]
    private Sprite UnderwaterRockSprite;

    [SerializeField]
    private Sprite RockSprite;

    [SerializeField]
    private Sprite DryingHeightSprite;

    [SerializeField]
    private Color Color = Color.blue;

    [SerializeField]
    private float SpriteSize = 10.0f;

    private float HalfSpriteSize => SpriteSize / 2;

    [SerializeField]
    private int FontSize = 50;

    [SerializeField]
    private DepthType[] Types = new DepthType[] { DepthType.DepthPoint, DepthType.UnderwaterRock, DepthType.Rock, DepthType.DryingHeight };

    [SerializeField]
    private GlobalSettings Settings;

    public async Task<DepthPoint[]> GetInfoPointsAsync(Database database, Vector2Int chunkCoordinates, int levelOfDetail, CancellationToken cancellationToken)
    {
        if (levelOfDetail > 0)
        {
            return Array.Empty<DepthPoint>();
        }

        var bottomLeft = chunkCoordinates * Settings.ChunkWidth;
        var topRight = bottomLeft + new Vector2Int(Settings.ChunkWidth, Settings.ChunkWidth);
        var infoPoints = await database.GetDepthWithinRectOfTypesAsync(bottomLeft, topRight, Types, cancellationToken);
        return infoPoints.ToArray();
    }

    public GameObject GenerateSprite(DepthPoint infoPoint)
    {
        var relativePosition = infoPoint.Position - Settings.Coordinates;
        var spriteObject = new GameObject($"{infoPoint.DepthType} ({relativePosition.x}, {relativePosition.y})");
        spriteObject.AddComponent<FaceCamera>();
        var y = -infoPoint.Depth + SpriteSize;
        spriteObject.transform.position = new(relativePosition.x, y, relativePosition.y);

        switch (infoPoint.DepthType)
        {
            case DepthType.DepthPoint:
                HandleDepthPoint(infoPoint, spriteObject);
                break;
            case DepthType.UnderwaterRock:
                HandleUnderwaterRock(infoPoint, spriteObject);
                break;
            case DepthType.Rock:
                HandleRock(infoPoint, spriteObject);
                break;
            case DepthType.DepthCurve:
                HandleDepthCurve(infoPoint, spriteObject);
                break;
            case DepthType.DryingHeight:
                HandleDryingHeight(infoPoint, spriteObject);
                break;
            case DepthType.Coastline:
                HandleCoastline(infoPoint, spriteObject);
                break;
        }

        return spriteObject;
    }

    private void HandleDepthPoint(DepthPoint infoPoint, GameObject spriteObject)
    {
        var text = AddText(spriteObject, infoPoint.Depth);
        text.fontStyle |= FontStyles.Italic;
    }

    private void HandleUnderwaterRock(DepthPoint infoPoint, GameObject spriteObject)
    {
        var text = AddText(spriteObject, infoPoint.Depth);

        if (infoPoint.Depth < 10)
        {
            AddImage(spriteObject, UnderwaterRockSprite);
            text.fontSize = FontSize / 2;
            text.rectTransform.SetLocalPositionAndRotation(new(HalfSpriteSize, -HalfSpriteSize), Quaternion.identity);
        }
    }

    private void HandleRock(DepthPoint infoPoint, GameObject spriteObject)
    {
        AddImage(spriteObject, RockSprite);
        var text = AddText(spriteObject, infoPoint.Depth);
        text.fontSize = FontSize / 2;
        text.rectTransform.SetLocalPositionAndRotation(new(HalfSpriteSize, -HalfSpriteSize), Quaternion.identity);
    }

    private void HandleDepthCurve(DepthPoint infoPoint, GameObject spriteObject)
    {

    }

    private void HandleDryingHeight(DepthPoint infoPoint, GameObject spriteObject)
    {
        AddImage(spriteObject, DryingHeightSprite);
        var text = AddText(spriteObject, infoPoint.Depth);
        text.fontSize = FontSize / 2;
        text.rectTransform.SetLocalPositionAndRotation(new(HalfSpriteSize, -HalfSpriteSize), Quaternion.identity);
    }

    private void HandleCoastline(DepthPoint infoPoint, GameObject spriteObject)
    {

    }

    private SVGImage AddImage(GameObject spriteObject, Sprite sprite)
    {
        var imageObject = new GameObject($"Image");
        imageObject.AddComponent<Canvas>();
        var image = imageObject.AddComponent<SVGImage>();
        image.sprite = sprite;
        image.color = Color;
        image.GetComponent<RectTransform>().sizeDelta = new(SpriteSize, SpriteSize);
        imageObject.transform.SetParent(spriteObject.transform, false);
        return image;
    }

    private TextMeshPro AddText(GameObject spriteObject, float depth)
    {
        var textObject = new GameObject($"Text");
        var text = textObject.AddComponent<TextMeshPro>();
        text.fontSize = FontSize;
        text.fontStyle |= FontStyles.Bold;
        text.color = Color;
        text.alignment = TextAlignmentOptions.Center;
        text.text = GetDepthText(depth);
        textObject.transform.SetParent(spriteObject.transform, false);
        return text;
    }

    private string GetDepthText(float depth)
    {
        if (depth > 10.5)
        {
            return ((int)depth).ToString();
        }
        else
        {
            return depth.ToString("F1", CultureInfo.InvariantCulture);
        }
    }
}

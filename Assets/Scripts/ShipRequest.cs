﻿public class Geometry
{
    public string type { get; set; } = "Polygon";
    public double[][][] coordinates { get; set; }
}

public class ShipRequest
{
    public ShipRequest(double[][] coordinates)
    {
        geometry = new()
        {
            coordinates = new double[][][]
            {
                coordinates
            }
        };
    }

    public Geometry geometry { get; set; }
    public string modelType { get; set; } = "Full";
    public bool includePosition { get; set; } = true;
    public bool includeStatic { get; set; } = true;
    public bool includeAton { get; set; } = false;
    public bool includeSafetyRelated { get; set; } = false;
    public bool includeBinaryBroadcastMetHyd { get; set; } = false;
    public bool downsample { get; set; } = false;
}

﻿public struct ClosestDepthResult
{
    public int Id;
    public int Sector;
    public double Distance;
    public double Depth;
}
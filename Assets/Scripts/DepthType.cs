﻿public enum DepthType
{
    DepthPoint,
    UnderwaterRock,
    Rock,
    DepthCurve,
    DryingHeight,
    Coastline
}

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    [SerializeField]
    private Material MeshMaterial;

    [SerializeField]
    private GlobalSettings Settings;

    private int[] _lodStepSizes;
    private int[][] _lodPerimeterStepSizes;
    private int[] _lodPaddings;
    private Vector2Int[][] _lodVertexPositions;
    private int[][] _lodTriangles;

    private void Start()
    {
        _lodStepSizes = new int[] { 1, 2, 4, 8 };

        _lodPerimeterStepSizes = new int[][]
        {
            new int[] { },
            new int[] { 2 },
            new int[] { 2, 4 },
            new int[] { 2, 4, 8 }
        };

        _lodPaddings = new int[]
        {
            0,
            2,
            6,
            14
        };

        _lodVertexPositions = Enumerable.Range(0, GlobalSettings.MAX_LOD + 1)
            .Select(lod => GenerateVertexPositions(lod))
            .ToArray();

        var lodVertexPositionsIndex = Enumerable.Range(0, GlobalSettings.MAX_LOD + 1)
            .Select(lod => GenerateVertexPositionsIndex(_lodVertexPositions[lod]))
            .ToArray();

        _lodTriangles = Enumerable.Range(0, GlobalSettings.MAX_LOD + 1)
            .Select(lod => GenerateTriangles(lodVertexPositionsIndex[lod], lod))
            .ToArray();
    }

    public Vector2Int[] GetVertexPositions(int levelOfDetail)
    {
        return _lodVertexPositions[levelOfDetail];
    }

    public GameObject GenerateMesh(double[] depths, int levelOfDetail)
    {
        var meshObject = new GameObject("Mesh");
        meshObject.AddComponent<MeshRenderer>().material = MeshMaterial;
        var meshFilter = meshObject.AddComponent<MeshFilter>();

        var vertexPositions = _lodVertexPositions[levelOfDetail];
        var triangles = _lodTriangles[levelOfDetail];
        Vector3[] vertices = new Vector3[vertexPositions.Length];

        for (int id = 0; id < vertexPositions.Length; id++)
        {
            var vertexPosition = vertexPositions[id];
            var depth = (float)depths[id];
            vertices[id] = new(vertexPosition.x * Settings.Resolution, -depth, vertexPosition.y * Settings.Resolution);
        }

        meshFilter.mesh = new();
        meshFilter.mesh.Clear();
        meshFilter.mesh.vertices = vertices;
        meshFilter.mesh.triangles = triangles;
        meshFilter.mesh.RecalculateNormals();

        return meshObject;
    }


    private Vector2Int[] GenerateVertexPositions(int levelOfdetail)
    {
        var vertexPositions = new List<Vector2Int>();
        var stepSizes = _lodPerimeterStepSizes[levelOfdetail];

        // Outer perimeter
        var outerPerimeter = Utils.GetPerimeter(new Vector2Int(0, 0), new Vector2Int(GlobalSettings.CHUNK_VERTEX_WIDTH, GlobalSettings.CHUNK_VERTEX_WIDTH));
        vertexPositions.AddRange(outerPerimeter);

        int padding = 0;

        // Perimeters
        foreach (var stepSize in stepSizes)
        {
            padding += stepSize;

            var start = new Vector2Int(padding, padding);
            var end = new Vector2Int(GlobalSettings.CHUNK_VERTEX_WIDTH - padding, GlobalSettings.CHUNK_VERTEX_WIDTH - padding);
            var perimeter = Utils.GetPerimeter(start, end, stepSize);
            vertexPositions.AddRange(perimeter);
        }

        padding += _lodStepSizes[levelOfdetail];

        // Inside
        for (int x = padding; x <= GlobalSettings.CHUNK_VERTEX_WIDTH - padding; x += _lodStepSizes[levelOfdetail])
        {
            for (int y = padding; y <= GlobalSettings.CHUNK_VERTEX_WIDTH - padding; y += _lodStepSizes[levelOfdetail])
            {
                var vertexPosition = new Vector2Int(x, y);
                vertexPositions.Add(vertexPosition);
            }
        }

        return vertexPositions.ToArray();
    }

    private Dictionary<Vector2Int, int> GenerateVertexPositionsIndex(Vector2Int[] vertexPositions)
    {
        var vertexPositionsIndex = new Dictionary<Vector2Int, int>();

        for (int id = 0; id < vertexPositions.Length; id++)
        {
            var vertexPosition = vertexPositions[id];
            vertexPositionsIndex[vertexPosition] = id;
        }

        return vertexPositionsIndex;
    }

    private int[] GenerateTriangles(Dictionary<Vector2Int, int> vertexPositionsIndex, int levelOfDetail)
    {
        var triangles = new List<int>();
        var padding = _lodPaddings[levelOfDetail];
        var stepSize = _lodStepSizes[levelOfDetail];

        // Inside
        for (int x = padding; x < GlobalSettings.CHUNK_VERTEX_WIDTH - padding; x += stepSize)
        {
            for (int y = padding; y < GlobalSettings.CHUNK_VERTEX_WIDTH - padding; y += stepSize)
            {
                var vertexPosition = new Vector2Int(x, y);

                triangles.AddRange(new int[]
                {
                    vertexPositionsIndex[vertexPosition],
                    vertexPositionsIndex[vertexPosition + new Vector2Int(0, stepSize)],
                    vertexPositionsIndex[vertexPosition + new Vector2Int(stepSize, stepSize)]
                });

                triangles.AddRange(new int[]
                {
                    vertexPositionsIndex[vertexPosition],
                    vertexPositionsIndex[vertexPosition + new Vector2Int(stepSize, stepSize)],
                    vertexPositionsIndex[vertexPosition + new Vector2Int(stepSize, 0)]
                });
            }
        }

        AddPerimetersTriangles(vertexPositionsIndex, levelOfDetail, triangles);

        return triangles.ToArray();
    }

    private void AddPerimetersTriangles(Dictionary<Vector2Int, int> vertexPositionsIndex, int levelOfDetail, List<int> triangles)
    {
        int padding = 0;

        var stepSizes = _lodPerimeterStepSizes[levelOfDetail];
        foreach (var stepSize in stepSizes)
        {
            padding += stepSize;

            AddCornerTriangles(vertexPositionsIndex, triangles, padding, stepSize);
            AddBottomTopTriangles(vertexPositionsIndex, triangles, padding, stepSize);
            AddLeftRightTriangles(vertexPositionsIndex, triangles, padding, stepSize);
        }
    }

    private void AddLeftRightTriangles(Dictionary<Vector2Int, int> vertexPositionsIndex, List<int> triangles, int padding, int stepSize)
    {
        for (int y = padding; y <= GlobalSettings.CHUNK_VERTEX_WIDTH - padding; y += stepSize)
        {
            var positionLeft = new Vector2Int(padding, y);
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionLeft],
                vertexPositionsIndex[positionLeft + new Vector2Int(-stepSize, 0)],
                vertexPositionsIndex[positionLeft + new Vector2Int(-stepSize, stepSize/2)]
            });
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionLeft],
                vertexPositionsIndex[positionLeft + new Vector2Int(-stepSize, -stepSize/2)],
                vertexPositionsIndex[positionLeft + new Vector2Int(-stepSize, 0)]
            });

            var positionRight = new Vector2Int(GlobalSettings.CHUNK_VERTEX_WIDTH - padding, y);
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionRight],
                vertexPositionsIndex[positionRight + new Vector2Int(stepSize, stepSize/2)],
                vertexPositionsIndex[positionRight + new Vector2Int(stepSize, 0)]
            });
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionRight],
                vertexPositionsIndex[positionRight + new Vector2Int(stepSize, 0)],
                vertexPositionsIndex[positionRight + new Vector2Int(stepSize, -stepSize/2)]
            });

            if (y < GlobalSettings.CHUNK_VERTEX_WIDTH - padding)
            {
                triangles.AddRange(new int[]
                {
                    vertexPositionsIndex[positionLeft],
                    vertexPositionsIndex[positionLeft + new Vector2Int(-stepSize, stepSize/2)],
                    vertexPositionsIndex[positionLeft + new Vector2Int(0, stepSize)]
                });
                triangles.AddRange(new int[]
                {
                    vertexPositionsIndex[positionRight],
                    vertexPositionsIndex[positionRight + new Vector2Int(0, stepSize)],
                    vertexPositionsIndex[positionRight + new Vector2Int(stepSize, stepSize/2)]
                });
            }
        }
    }

    private void AddBottomTopTriangles(Dictionary<Vector2Int, int> vertexPositionsIndex, List<int> triangles, int padding, int stepSize)
    {
        for (int x = padding; x <= GlobalSettings.CHUNK_VERTEX_WIDTH - padding; x += stepSize)
        {
            var positionBottom = new Vector2Int(x, padding);
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionBottom],
                vertexPositionsIndex[positionBottom + new Vector2Int(stepSize/2, -stepSize)],
                vertexPositionsIndex[positionBottom + new Vector2Int(0, -stepSize)]
            });
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionBottom],
                vertexPositionsIndex[positionBottom + new Vector2Int(0, -stepSize)],
                vertexPositionsIndex[positionBottom + new Vector2Int(-stepSize/2, -stepSize)]
            });

            var positionTop = new Vector2Int(x, GlobalSettings.CHUNK_VERTEX_WIDTH - padding);
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionTop],
                vertexPositionsIndex[positionTop + new Vector2Int(0, stepSize)],
                vertexPositionsIndex[positionTop + new Vector2Int(stepSize/2, stepSize)]
            });
            triangles.AddRange(new int[]
            {
                vertexPositionsIndex[positionTop],
                vertexPositionsIndex[positionTop + new Vector2Int(-stepSize/2, stepSize)],
                vertexPositionsIndex[positionTop + new Vector2Int(0, stepSize)]
            });

            if (x < GlobalSettings.CHUNK_VERTEX_WIDTH - padding)
            {
                triangles.AddRange(new int[]
                {
                    vertexPositionsIndex[positionBottom],
                    vertexPositionsIndex[positionBottom + new Vector2Int(stepSize, 0)],
                    vertexPositionsIndex[positionBottom + new Vector2Int(stepSize/2, -stepSize)]
                });
                triangles.AddRange(new int[]
                {
                    vertexPositionsIndex[positionTop],
                    vertexPositionsIndex[positionTop + new Vector2Int(stepSize/2, stepSize)],
                    vertexPositionsIndex[positionTop + new Vector2Int(stepSize, 0)]
                });
            }
        }
    }

    private void AddCornerTriangles(Dictionary<Vector2Int, int> vertexPositionsIndex, List<int> triangles, int padding, int stepSize)
    {
        var positionBottomLeft = new Vector2Int(padding, padding);
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionBottomLeft],
            vertexPositionsIndex[positionBottomLeft + new Vector2Int(-stepSize/2, -stepSize)],
            vertexPositionsIndex[positionBottomLeft + new Vector2Int(-stepSize, -stepSize)]
        });
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionBottomLeft],
            vertexPositionsIndex[positionBottomLeft + new Vector2Int(-stepSize, -stepSize)],
            vertexPositionsIndex[positionBottomLeft + new Vector2Int(-stepSize, -stepSize/2)]
        });

        var positionTopLeft = new Vector2Int(padding, GlobalSettings.CHUNK_VERTEX_WIDTH - padding);
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionTopLeft],
            vertexPositionsIndex[positionTopLeft + new Vector2Int(-stepSize, stepSize)],
            vertexPositionsIndex[positionTopLeft + new Vector2Int(-stepSize/2, stepSize)]
        });
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionTopLeft],
            vertexPositionsIndex[positionTopLeft + new Vector2Int(-stepSize, stepSize/2)],
            vertexPositionsIndex[positionTopLeft + new Vector2Int(-stepSize, stepSize)]
        });

        var positionBottomRight = new Vector2Int(GlobalSettings.CHUNK_VERTEX_WIDTH - padding, padding);
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionBottomRight],
            vertexPositionsIndex[positionBottomRight + new Vector2Int(stepSize, -stepSize)],
            vertexPositionsIndex[positionBottomRight + new Vector2Int(stepSize/2, -stepSize)]
        });
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionBottomRight],
            vertexPositionsIndex[positionBottomRight + new Vector2Int(stepSize, -stepSize/2)],
            vertexPositionsIndex[positionBottomRight + new Vector2Int(stepSize, -stepSize)]
        });

        var positionTopRight = new Vector2Int(GlobalSettings.CHUNK_VERTEX_WIDTH - padding, GlobalSettings.CHUNK_VERTEX_WIDTH - padding);
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionTopRight],
            vertexPositionsIndex[positionTopRight + new Vector2Int(stepSize/2, stepSize)],
            vertexPositionsIndex[positionTopRight + new Vector2Int(stepSize, stepSize)]
        });
        triangles.AddRange(new int[]
        {
            vertexPositionsIndex[positionTopRight],
            vertexPositionsIndex[positionTopRight + new Vector2Int(stepSize, stepSize)],
            vertexPositionsIndex[positionTopRight + new Vector2Int(stepSize, stepSize/2)]
        });
    }
}
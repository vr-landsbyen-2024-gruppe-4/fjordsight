﻿using UnityEngine;

struct DepthChunkResult
{
    public double[] Depths;
    public Vector2Int ChunkCoordinates;
    public int LevelOfDetail;
    public DepthPoint[] InfoPoints;
}

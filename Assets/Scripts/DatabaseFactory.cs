using Npgsql;
using System.Threading;
using System.Threading.Tasks;

public class DatabaseFactory
{
    private readonly NpgsqlDataSource _dataSource;
    public DatabaseFactory()
    {
        var connectionString = "Host=localhost;Username=read_user;Password=read_user;Database=depthdata;Timeout=60";
        var dataSourceBuilder = new NpgsqlDataSourceBuilder(connectionString);
        _dataSource = dataSourceBuilder.Build();
    }

    public async Task<Database> GetDatabaseAsync(CancellationToken cancellationToken)
    {
        var connection = await _dataSource.OpenConnectionAsync(cancellationToken);
        return new(connection);
    }
}
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class Database : IDisposable
{
    private readonly NpgsqlConnection _connection;

    public Database(NpgsqlConnection connection)
    {
        _connection = connection;
    }

    public async Task<List<DepthPoint>> GetDepthWithinRectAsync(Vector2Int bottomLeft, Vector2Int topRight, CancellationToken cancellationToken)
    {
        string rawCommand = $@"
            SELECT
            ST_X(position) as x,
            ST_Y(position) as y,
            depth,
            depthtype
            FROM depth
            WHERE ST_Within(position,
	            ST_MakeEnvelope (
		            {bottomLeft.x},
		            {bottomLeft.y},
		            {topRight.x},
		            {topRight.y},
		            25833
	            )
            )
        ";
        await using var command = new NpgsqlCommand(rawCommand, _connection);

        List<DepthPoint> values = new();

        using var reader = await command.ExecuteReaderAsync(cancellationToken);
        while (await reader.ReadAsync(cancellationToken))
        {
            var x = (float)reader.GetDouble(0);
            var y = (float)reader.GetDouble(1);
            var depth = (float)reader.GetDouble(2);
            var depthType = reader.GetInt32(3);

            values.Add(new DepthPoint
            {
                Position = new(x, y),
                Depth = depth,
                DepthType = (DepthType)depthType
            });
        }
        await reader.CloseAsync();

        return values;
    }

    public async Task<List<DepthPoint>> GetDepthWithinRectOfTypesAsync(Vector2Int bottomLeft, Vector2Int topRight, DepthType[] types, CancellationToken cancellationToken)
    {
        string typeFilter =  string.Join(" OR ", types.Cast<int>().Select(t => $"depthtype = {t}"));

        string rawCommand = $@"
            SELECT
            ST_X(position) as x,
            ST_Y(position) as y,
            depth,
            depthtype
            FROM depth
            WHERE ST_Within(position,
	            ST_MakeEnvelope (
		            {bottomLeft.x},
		            {bottomLeft.y},
		            {topRight.x},
		            {topRight.y},
		            25833
	            )
            ) AND ({typeFilter})
        ";

        await using var command = new NpgsqlCommand(rawCommand, _connection);

        List<DepthPoint> values = new();

        using var reader = await command.ExecuteReaderAsync(cancellationToken);
        while (await reader.ReadAsync(cancellationToken))
        {
            var x = (float)reader.GetDouble(0);
            var y = (float)reader.GetDouble(1);
            var depth = (float)reader.GetDouble(2);
            var depthType = reader.GetInt32(3);

            values.Add(new DepthPoint
            {
                Position = new(x, y),
                Depth = depth,
                DepthType = (DepthType)depthType
            });
        }
        await reader.CloseAsync();

        return values;
    }

    public async Task<List<ClosestDepthResult>> GetClosestDepthAsync(Dictionary<int, Vector2Int> points, CancellationToken cancellationToken)
    {
        await using var batch = new NpgsqlBatch(_connection);

        Dictionary<int, int> sectors = new();

        foreach (var (id, point) in points)
        {
            string rawCommand = $@"
                    SELECT
                        {id} AS id,
	                    position <-> ST_SetSRID(ST_MakePoint({point.x}, {point.y}), 25833) AS distance,
	                    depth
                    FROM depth
                    ORDER BY distance
                    LIMIT 4
                ";
            var command = new NpgsqlBatchCommand(rawCommand);
            batch.BatchCommands.Add(command);

            sectors[id] = 0;
        }

        List<ClosestDepthResult> values = new();

        using var reader = await batch.ExecuteReaderAsync(cancellationToken);

        while (await reader.NextResultAsync(cancellationToken))
        {
            while (await reader.ReadAsync(cancellationToken))
            {
                var id = reader.GetInt32(0);
                var distance = reader.GetDouble(1);
                var depth = reader.GetDouble(2);

                values.Add(new ClosestDepthResult
                {
                    Id = id,
                    Sector = sectors[id],
                    Distance = distance,
                    Depth = depth
                });

                sectors[id]++;
            }
        }
        await reader.CloseAsync();

        return values;
    }

    public async Task<bool> AnyWithinAsync(Vector2Int start, Vector2Int end, CancellationToken cancellationToken)
    {
        string rawCommand = $@"
            SELECT * FROM depth
            WHERE ST_Within(position, ST_MakeEnvelope({start.x}, {start.y}, {end.x}, {end.y}, 25833))
            LIMIT 1;";
        await using var command = new NpgsqlCommand(rawCommand, _connection);

        var result = await command.ExecuteScalarAsync(cancellationToken);
        return result != null;
    }

    public void Dispose()
    {
        _connection.Close();
    }
}
using UnityEngine;

public class MapArrow : MonoBehaviour
{
    [SerializeField]
    private Transform CameraTransform;

    void Update()
    {
        transform.SetAsLastSibling();

        var forward = CameraTransform.forward;
        forward.y = 0;
        var angle = Vector3.SignedAngle(forward.normalized, Vector3.forward, Vector3.up);

        transform.localRotation = Quaternion.identity;
        transform.Rotate(Vector3.forward, angle);
    }
}

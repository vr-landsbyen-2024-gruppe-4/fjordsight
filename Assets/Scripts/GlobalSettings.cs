﻿using UnityEngine;

public class GlobalSettings : MonoBehaviour
{
    public const int CHUNK_VERTEX_WIDTH = 60;
    public const int MAX_LOD = 3;

    public int RenderDistance = 5000;
    public int Resolution = 4;
    public Vector2Int Coordinates = new(47376, 6457912);
    public Transform UserTransform;

    public int ChunkWidth => CHUNK_VERTEX_WIDTH * Resolution;
}

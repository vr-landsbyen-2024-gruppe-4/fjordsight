using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity.Mathematics;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    [SerializeField]
    private GlobalSettings Settings;

    [SerializeField]
    private Ship ShipPrefab;

    [SerializeField]
    private int UpdatePeriodSeconds = 60;

    private DateTime _lastUpdated = DateTime.MinValue;
    private ShipClient _client;
    private CoordinateTransformer _transformer;

    private readonly Dictionary<int, Ship> _ships = new();

    private void Start()
    {
        _client = new();
        _transformer = new();
    }

    private async void Update()
    {
        var timeSinceUpdate = DateTime.UtcNow - _lastUpdated;
        if (timeSinceUpdate > TimeSpan.FromSeconds(UpdatePeriodSeconds))
        {
            _lastUpdated = DateTime.UtcNow;

            await UpdateShipsAsync();
            DeleteFarAwayShips();
        }
    }

    private void DeleteFarAwayShips()
    {
        var toBeRemoved = _ships.Where(x =>
        {
            var vec = Settings.UserTransform.position - x.Value.transform.position;
            return vec.magnitude > Settings.RenderDistance;
        }).ToList();

        foreach (var (mmsi, ship) in toBeRemoved)
        {
            Destroy(ship.gameObject);
            _ships.Remove(mmsi);
        }
    }

    private async Task UpdateShipsAsync()
    {
        Vector2 center = Settings.Coordinates;
        center.x += Settings.UserTransform.position.x;
        center.y += Settings.UserTransform.position.z;

        var renderDistance = new Vector2(Settings.RenderDistance, Settings.RenderDistance);

        var bottomLeft = _transformer.FromEpsg25833ToWgs84(center - renderDistance);
        var topRight = _transformer.FromEpsg25833ToWgs84(center + renderDistance);
        var shipResponses = await _client.GetShipsAsync(bottomLeft, topRight);

        foreach (var response in shipResponses)
        {
            if (response.latitude == null || response.longitude == null)
            {
                continue;
            }

            if (!_ships.TryGetValue(response.mmsi, out var ship))
            {
                ship = Instantiate(ShipPrefab, transform);
                _ships.Add(response.mmsi, ship);
            }

            var shipData = MapToShipData(response);
            ship.SetShipData(shipData);
        }
    }

    private ShipData MapToShipData(ShipResponse response)
    {
        var name = response.name ?? "";

        var wgs84 = new Vector2((float)response.longitude, (float)response.latitude);
        var coords = _transformer.FromWgs84ToEpsg25833(wgs84) - Settings.Coordinates;

        var width = response.shipWidth ?? 1;
        var length = response.shipLength ?? 1;
        var height = math.min(width, length);
        var size = new Vector3(width, height, length);

        var position = new Vector3(coords.x, 0, coords.y);

        float rotation = (float)(response.trueHeading ?? (response.courseOverGround ?? 0));

        var offset = new Vector3(
            (response.dimensionC ?? width / 2) / width,
            0,
            (response.dimensionB ?? length / 2) / length
        );

        var speed = (float)(response.speedOverGround ?? 0);

        var directionDegrees = (float)(response.courseOverGround ?? rotation);
        var directionRadians = 2 * MathF.PI / 360 * directionDegrees;

        var direction = new Vector3(
            Mathf.Sin(directionRadians),
            0,
            Mathf.Cos(directionRadians)
        );

        var angularSpeed = (float)(response.speedOverGround ?? 0);

        return new ShipData
        {
            Name = name,
            Size = size,
            Position = position,
            Rotation = rotation,
            Offset = offset,
            Speed = speed,
            Direction = direction,
            AngularSpeed = angularSpeed,
        };
    }
}

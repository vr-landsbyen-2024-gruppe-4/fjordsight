﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Unity.Mathematics;
using UnityEngine;

[RequireComponent(typeof(DepthGenerator), typeof(MeshGenerator), typeof(InfoPointGenerator))]
public class ChunkManager : MonoBehaviour
{
    [SerializeField]
    private int ParallelCount = 16;

    private int DeleteDistance => Settings.RenderDistance + Settings.ChunkWidth;

    [SerializeField]
    private int DistancePerLOD = 250;

    [SerializeField]
    [Range(0, GlobalSettings.MAX_LOD)]
    private int MaxLOD = GlobalSettings.MAX_LOD;

    [SerializeField]
    private GlobalSettings Settings;

    private Vector2Int UserPosition => new(
        Settings.Coordinates.x + (int)Settings.UserTransform.position.x,
        Settings.Coordinates.y + (int)Settings.UserTransform.position.z
    );

    private DatabaseFactory _databaseFactory;
    private DepthGenerator _depthGenerator;
    private MeshGenerator _meshGenerator;
    private InfoPointGenerator _infoPointGenerator;

    private CancellationTokenSource _cancellationTokenSource;
    private int _chunkGenerationCount = 0;
    private readonly Dictionary<Vector2Int, int> _requestedChunks = new();
    private readonly Dictionary<Vector2Int, (GameObject chunk, int lod)> _chunks = new();
    private readonly ConcurrentQueue<DepthChunkResult> _finished = new();

    private void Start()
    {
        _databaseFactory = new();

        _depthGenerator = GetComponent<DepthGenerator>();
        _meshGenerator = GetComponent<MeshGenerator>();
        _infoPointGenerator = GetComponent<InfoPointGenerator>();

        _cancellationTokenSource = new();
    }

    private void Update()
    {
        Camera.main.farClipPlane = Settings.RenderDistance;

        DeleteFarAwayChunks();
        GenerateChunksForFinishedDepths();
        StartNewDepthGenerationThreads();
    }

    private void StartNewThread(Vector2Int chunkCoordinates, int levelOfDetail)
    {
        new Thread(async () =>
        {
            using var database = await _databaseFactory.GetDatabaseAsync(_cancellationTokenSource.Token);

            var depths = await _depthGenerator.GenerateDepthsAsync(
                database,
                chunkCoordinates,
                levelOfDetail,
                _cancellationTokenSource.Token
            );

            var infoPoints = await _infoPointGenerator.GetInfoPointsAsync(
                database,
                chunkCoordinates,
                levelOfDetail,
                _cancellationTokenSource.Token
            );

            _finished.Enqueue(new DepthChunkResult
            {
                Depths = depths,
                InfoPoints = infoPoints,
                ChunkCoordinates = chunkCoordinates,
                LevelOfDetail = levelOfDetail
            });
        }).Start();
    }

    private void StartNewDepthGenerationThreads()
    {
        var centerChunkCoordinates = UserPosition / Settings.ChunkWidth;
        var renderChunkDistance = Settings.RenderDistance / Settings.ChunkWidth;
        var chunksPerLOD = DistancePerLOD / Settings.ChunkWidth;

        for (int r = 0; r <= renderChunkDistance; r++)
        {
            foreach (var relativeChunkCoordinates in Utils.GetPerimeter(new Vector2Int(-r, -r), new Vector2Int(r, r)))
            {
                if (_chunkGenerationCount >= ParallelCount)
                {
                    return;
                }

                if (relativeChunkCoordinates.magnitude * Settings.ChunkWidth > Settings.RenderDistance)
                {
                    continue;
                }

                var chunkCoordinates = centerChunkCoordinates + relativeChunkCoordinates;
                var levelOfDetail = (int)math.min(relativeChunkCoordinates.magnitude / chunksPerLOD, MaxLOD);
                var requestedLod = _requestedChunks.GetValueOrDefault(chunkCoordinates, MaxLOD + 2);
                var lodDelta = levelOfDetail - requestedLod;

                if (lodDelta != 0 && lodDelta != 1)
                {
                    _requestedChunks[chunkCoordinates] = levelOfDetail;
                    _chunkGenerationCount++;
                    StartNewThread(chunkCoordinates, levelOfDetail);
                }
            }
        }
    }

    private void GenerateChunksForFinishedDepths()
    {
        var centerChunkCoordinates = UserPosition / Settings.ChunkWidth;
        var chunksPerLOD = DistancePerLOD / Settings.ChunkWidth;

        while (_finished.TryDequeue(out var result))
        {
            _chunkGenerationCount--;

            var (existingChunk, existingLod) = _chunks.GetValueOrDefault(result.ChunkCoordinates, (null, MaxLOD + 2));

            var relativeChunkCoordinates = result.ChunkCoordinates - centerChunkCoordinates;
            var wantedLevelOfDetail = (int)math.min(relativeChunkCoordinates.magnitude / chunksPerLOD, MaxLOD);

            var resultDelta = math.abs(wantedLevelOfDetail - result.LevelOfDetail);
            var existingDelta = math.abs(wantedLevelOfDetail - existingLod);

            if (resultDelta <= existingDelta)
            {
                Destroy(existingChunk);
                GenerateChunk(result);
            }
        }
    }

    private void GenerateChunk(DepthChunkResult result)
    {
        var chunkObject = new GameObject($"Chunk ({result.ChunkCoordinates.x}, {result.ChunkCoordinates.y})");
        var chunkWorldPosition = Settings.ChunkWidth * result.ChunkCoordinates;
        var relativePosition = chunkWorldPosition - Settings.Coordinates;
        chunkObject.transform.position = new(relativePosition.x, 0, relativePosition.y);
        chunkObject.transform.SetParent(transform);

        GameObject meshObject = _meshGenerator.GenerateMesh(result.Depths, result.LevelOfDetail);
        meshObject.transform.SetParent(chunkObject.transform, false);

        foreach (var infoPoints in result.InfoPoints)
        {
            var spriteObject = _infoPointGenerator.GenerateSprite(infoPoints);
            spriteObject.transform.SetParent(chunkObject.transform);
        }

        _chunks[result.ChunkCoordinates] = (chunkObject, result.LevelOfDetail);
    }

    private void DeleteFarAwayChunks()
    {
        var centerChunkCoordinates = UserPosition / Settings.ChunkWidth;

        var toBeDeleted = _chunks.Where((pair) =>
        {
            var (chunkCoordinates, _) = pair;
            var relativeChunkCoordinates = centerChunkCoordinates - chunkCoordinates;
            return relativeChunkCoordinates.magnitude * Settings.ChunkWidth > DeleteDistance;
        }).ToList();

        foreach (var (chunkCoordinates, (chunk, _)) in toBeDeleted)
        {
            Destroy(chunk);
            _chunks.Remove(chunkCoordinates);
            _requestedChunks.Remove(chunkCoordinates);
        }
    }

    private void OnApplicationQuit()
    {
        _cancellationTokenSource?.Cancel();
    }
}

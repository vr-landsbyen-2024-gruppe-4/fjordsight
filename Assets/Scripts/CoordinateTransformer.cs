using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;
using UnityEngine;

public class CoordinateTransformer
{

    private const string wgs84Wkt = @"
GEOGCS[""WGS 84"",
    DATUM[""WGS_1984"",
        SPHEROID[""WGS 84"",6378137,298.257223563,
            AUTHORITY[""EPSG"",""7030""]],
        AUTHORITY[""EPSG"",""6326""]],
    PRIMEM[""Greenwich"",0,
        AUTHORITY[""EPSG"",""8901""]],
    UNIT[""degree"",0.0174532925199433,
        AUTHORITY[""EPSG"",""9122""]],
    AUTHORITY[""EPSG"",""4326""]]
";

    private const string epsg25833Wkt = @"
PROJCS[""ETRS89 / UTM zone 33N"",
    GEOGCS[""ETRS89"",
        DATUM[""European_Terrestrial_Reference_System_1989"",
            SPHEROID[""GRS 1980"",6378137,298.257222101,
                AUTHORITY[""EPSG"",""7019""]],
            TOWGS84[0,0,0,0,0,0,0],
            AUTHORITY[""EPSG"",""6258""]],
        PRIMEM[""Greenwich"",0,
            AUTHORITY[""EPSG"",""8901""]],
        UNIT[""degree"",0.0174532925199433,
            AUTHORITY[""EPSG"",""9122""]],
        AUTHORITY[""EPSG"",""4258""]],
    PROJECTION[""Transverse_Mercator""],
    PARAMETER[""latitude_of_origin"",0],
    PARAMETER[""central_meridian"",15],
    PARAMETER[""scale_factor"",0.9996],
    PARAMETER[""false_easting"",500000],
    PARAMETER[""false_northing"",0],
    UNIT[""metre"",1,
        AUTHORITY[""EPSG"",""9001""]],
    AXIS[""Easting"",EAST],
    AXIS[""Northing"",NORTH],
    AUTHORITY[""EPSG"",""25833""]]
";

    private readonly MathTransform _wgs84ToEpsg25833Transformation;
    private readonly MathTransform _epsg25833ToWgs84Transformation;

    public CoordinateTransformer()
    {
        var coordinateSystemfactory = new CoordinateSystemFactory();
        var wgs84 = coordinateSystemfactory.CreateFromWkt(wgs84Wkt);
        var epsg25833 = coordinateSystemfactory.CreateFromWkt(epsg25833Wkt);

        var transformationFactory = new CoordinateTransformationFactory();
        _wgs84ToEpsg25833Transformation = transformationFactory.CreateFromCoordinateSystems(wgs84, epsg25833).MathTransform;
        _epsg25833ToWgs84Transformation = transformationFactory.CreateFromCoordinateSystems(epsg25833, wgs84).MathTransform;
    }

    public Vector2 FromWgs84ToEpsg25833(Vector2 wgs84)
    {
        var (x, y) = _wgs84ToEpsg25833Transformation.Transform(wgs84.x, wgs84.y);
        return new((float)x, (float)y);
    }

    public Vector2 FromEpsg25833ToWgs84(Vector2 epsg25833)
    {
        var (x, y) = _epsg25833ToWgs84Transformation.Transform(epsg25833.x, epsg25833.y);
        return new((float)x, (float)y);
    }
}
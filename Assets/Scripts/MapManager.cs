using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

struct Index
{
    public int Column;
    public int Row;
    public int Zoom;
}

[RequireComponent(typeof(Canvas))]
public class MapManager : MonoBehaviour
{
    private const double ORIGIN_X = -2500000;
    private const double ORIGIN_Y = 9045984;

    private const double BASE_RESOLUTION = 77371428.57142858207225799561 * 0.00028;
    private const int IMAGE_WIDTH = 256;

    [SerializeField]
    private int MinZoom = 3;

    [SerializeField]
    private int MaxZoom = 16;

    [SerializeField]
    private int StartZoom = 12;

    [SerializeField]
    private GlobalSettings Settings;

    private Vector2 UserPosition => new(
        Settings.Coordinates.x + Settings.UserTransform.position.x,
        Settings.Coordinates.y + Settings.UserTransform.position.z
    );

    private int _zoom;
    private RectTransform _rectTransform;
    private HttpClient _client;

    private readonly Dictionary<Index, Task<Texture2D>> _tasks = new();

    private readonly Dictionary<Index, Texture2D> _textures = new();

    private readonly Dictionary<Index, GameObject> _activeImageObjects = new();

    private void Start()
    {
        _zoom = StartZoom;

        _rectTransform = GetComponent<RectTransform>();

        _client = new()
        {
            BaseAddress = new("https://cache.kartverket.no/sjokartraster/v1/wmts/1.0.0/")
        };
    }

    public void ZoomIn()
    {
        Zoom(1);
    }

    public void ZoomOut()
    {
        Zoom(-1);
    }

    private void Zoom(int y)
    {
        var newZoom = math.clamp(_zoom + y, MinZoom, MaxZoom);

        if (_zoom != newZoom)
        {
            foreach (var image in _activeImageObjects.Values.ToList())
            {
                Destroy(image);
            }
            _activeImageObjects.Clear();
            _zoom = newZoom;
        }
    }

    private async void Update()
    {
        await FinishTasksAsync();
        UpdateImages();
    }

    private async Task FinishTasksAsync()
    {
        var finishedTasks = _tasks.Where(t => t.Value.IsCompleted).ToList();
        foreach (var (index, task) in finishedTasks)
        {
            var texture = await task;
            _textures.Add(index, texture);
            _tasks.Remove(index);
        }
    }

    private void UpdateImages()
    {
        var centerPosition = CalculatePosition(UserPosition);

        var centerIndex = CalculateIndex(UserPosition);
        var halfNumImages = _rectTransform.sizeDelta / (2 * IMAGE_WIDTH);
        int halfNumImagesX = (int)halfNumImages.x + 1;
        int halfNumImagesY = (int)halfNumImages.y + 1;

        for (int dx = -halfNumImagesX; dx <= halfNumImagesX; dx++)
        {
            for (int dy = -halfNumImagesY; dy <= halfNumImagesY; dy++)
            {
                var index = new Index
                {
                    Column = centerIndex.Column + dx,
                    Row = centerIndex.Row + dy,
                    Zoom = centerIndex.Zoom
                };

                var isActive = _activeImageObjects.ContainsKey(index);
                var isTexture = _textures.ContainsKey(index);
                var isStartedTask = _tasks.ContainsKey(index);

                // If not texture and not started: start and return
                if (!isTexture && !isStartedTask)
                {
                    var task = GetImageTextureAsync(index);
                    _tasks.Add(index, task);
                    return;
                }

                // If not active, but finished texture: Make image
                if (!isActive && isTexture)
                {
                    var texture = _textures[index];
                    _activeImageObjects[index] = MakeImage(texture);
                }

                // If image: update image
                if (_activeImageObjects.TryGetValue(index, out var image))
                {
                    image.transform.SetParent(transform, false);
                    image.transform.localScale = Vector3.one;
                    image.transform.localRotation = Quaternion.identity;

                    var position = new Vector2(
                        index.Column - centerPosition.x + 0.5f,
                        centerPosition.y - index.Row - 0.5f
                    ) * IMAGE_WIDTH;
                    image.GetComponent<RawImage>().rectTransform.localPosition = position;
                }
            }
        }
    }

    private async Task<Texture2D> GetImageTextureAsync(Index index)
    {
        var request = GetRequest(index);
        var response = await _client.GetAsync(request);
        var imageData = await response.Content.ReadAsByteArrayAsync();

        Texture2D texture = new(IMAGE_WIDTH, IMAGE_WIDTH);
        texture.LoadImage(imageData);

        return texture;
    }

    private GameObject MakeImage(Texture2D texture)
    {
        var image = new GameObject("image");
        image.transform.SetParent(transform);

        var rawImage = image.AddComponent<RawImage>();
        rawImage.rectTransform.sizeDelta = new(IMAGE_WIDTH, IMAGE_WIDTH);
        rawImage.rectTransform.anchoredPosition = new();
        rawImage.texture = texture;

        return image;
    }

    private string GetRequest(Index index)
    {
        return $"?layer=sjokartraster&style=default&tilematrixset=utm33n&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image/png&TileMatrix={index.Zoom}&TileCol={index.Column}&TileRow={index.Row}";
    }

    private Index CalculateIndex(Vector2 coordinates)
    {
        var position = CalculatePosition(coordinates);

        return new Index
        {
            Column = (int)position.x,
            Row = (int)position.y,
            Zoom = _zoom
        };
    }

    private Vector2 CalculatePosition(Vector2 coordinates)
    {
        double dx = (coordinates.x - ORIGIN_X);
        double dy = (ORIGIN_Y - coordinates.y);

        dx *= 1 << _zoom;
        dy *= 1 << _zoom;

        dx /= BASE_RESOLUTION;
        dy /= BASE_RESOLUTION;

        dx /= IMAGE_WIDTH;
        dy /= IMAGE_WIDTH;

        return new((float)dx, (float)dy);
    }
}
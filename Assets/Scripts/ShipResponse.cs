﻿using System;

public class ShipResponse
{
    public int mmsi { get; set; }
    public DateTime msgtime { get; set; }
    public int? altitude { get; set; }
    public double? courseOverGround { get; set; }
    public double? latitude { get; set; }
    public double? longitude { get; set; }
    public int navigationalStatus { get; set; }
    public double? rateOfTurn { get; set; }
    public double? speedOverGround { get; set; }
    public int? trueHeading { get; set; }
    public int? imoNumber { get; set; }
    public string? callSign { get; set; }
    public string? name { get; set; }
    public string? destination { get; set; }
    public string? eta { get; set; }
    public int? draught { get; set; }
    public int? shipLength { get; set; }
    public int? shipWidth { get; set; }
    public int? shipType { get; set; }
    public int? dimensionA { get; set; }
    public int? dimensionB { get; set; }
    public int? dimensionC { get; set; }
    public int? dimensionD { get; set; }
    public int positionFixingDeviceType { get; set; }
    public string reportClass { get; set; }
    public DateTime msgtimeStatic { get; set; }
}

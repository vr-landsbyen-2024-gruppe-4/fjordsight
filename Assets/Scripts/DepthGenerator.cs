﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

[RequireComponent(typeof(MeshGenerator))]
public class DepthGenerator : MonoBehaviour
{
    [SerializeField]
    private int SearchStartRadius = 256;

    [SerializeField]
    private int SearchEndRadius = 2048;

    [SerializeField]
    private GlobalSettings Settings;

    private MeshGenerator _meshGenerator;

    private void Start()
    {
        _meshGenerator = GetComponent<MeshGenerator>();
    }

    public async Task<double[]> GenerateDepthsAsync(Database database, Vector2Int chunkCoordinates, int levelOfDetail, CancellationToken cancellationToken)
    {
        var vertexPositions = _meshGenerator.GetVertexPositions(levelOfDetail);

        if (await IsCompletelyLandAsync(database, chunkCoordinates, cancellationToken))
        {
            return new double[vertexPositions.Length];
        }

        var worldPositions = GenerateWorldPositions(vertexPositions, chunkCoordinates);
        var values = await GetValuesAsync(database, worldPositions, cancellationToken);
        return CalculateEstimates(values);
    }

    private async Task<bool> IsCompletelyLandAsync(Database database, Vector2Int chunkCoordinates, CancellationToken cancellationToken)
    {
        var bottomLeft = chunkCoordinates * Settings.ChunkWidth - Vector2Int.one;
        var bottomRight = bottomLeft + new Vector2Int(GlobalSettings.CHUNK_VERTEX_WIDTH + 2, 0) * Settings.Resolution;
        var topLeft = bottomLeft + new Vector2Int(0, GlobalSettings.CHUNK_VERTEX_WIDTH + 2) * Settings.Resolution;
        var topRight = bottomLeft + new Vector2Int(GlobalSettings.CHUNK_VERTEX_WIDTH + 2, GlobalSettings.CHUNK_VERTEX_WIDTH + 2) * Settings.Resolution;

        var anyWithin = await database.AnyWithinAsync(bottomLeft, topRight, cancellationToken);

        if (anyWithin)
        {
            return false;
        }

        Dictionary<int, Vector2Int> points = new()
        {
            {0,  bottomLeft},
            {1,  bottomRight},
            {2,  topLeft},
            {3,  topRight},
        };

        var depthResults = await database.GetClosestDepthAsync(points, cancellationToken);
        return depthResults.All(dr => dr.Depth <= 0);
    }

    private Vector2Int[] GenerateWorldPositions(Vector2Int[] vertexPositions, Vector2Int chunkCoordinates)
    {
        var bottomLeft = chunkCoordinates * Settings.ChunkWidth;
        Vector2Int[] worldPositions = new Vector2Int[vertexPositions.Length];

        for (int id = 0; id < vertexPositions.Length; id++)
        {
            var vertexPosition = vertexPositions[id];
            var worldPosition = bottomLeft + vertexPosition * Settings.Resolution;
            worldPositions[id] = worldPosition;
        }

        return worldPositions;
    }

    private async Task<DistanceDepth[,]> GetValuesAsync(Database database, Vector2Int[] worldPositions, CancellationToken cancellationToken)
    {
        DistanceDepth[,] values = new DistanceDepth[worldPositions.Length, 4];
        for (int id = 0; id < worldPositions.Length; id++)
        {
            for (int sector = 0; sector < 4; sector++)
            {
                values[id, sector].Distance = -1;
            }
        }

        int[] pointCount = new int[worldPositions.Length];

        var bottomLeft = worldPositions[0];
        var topRight = bottomLeft + new Vector2Int(Settings.ChunkWidth, Settings.ChunkWidth);

        List<DepthPoint> depthResults = new();

        var searchRadius = SearchStartRadius;
        while (pointCount.Any(x => x == 0) || (searchRadius <= SearchEndRadius && pointCount.Any(x => x < 4)))
        {
            var min = bottomLeft - new Vector2Int(searchRadius, searchRadius);
            var max = topRight + new Vector2Int(searchRadius, searchRadius);

            depthResults = await database.GetDepthWithinRectAsync(min, max, cancellationToken);

            for (int id = 0; id < worldPositions.Length; id++)
            {
                if (pointCount[id] == 4)
                {
                    continue;
                }

                var position = worldPositions[id];
                float[] recordDistanceSquared = new float[] { -1, -1, -1, -1 };
                float[] recordDepth = new float[4];

                foreach (var depthResult in depthResults)
                {
                    int sector = CalculateSector(position, depthResult);
                    var distanceSquared = (position - depthResult.Position).sqrMagnitude;

                    if (recordDistanceSquared[sector] < 0 || distanceSquared < recordDistanceSquared[sector])
                    {
                        recordDistanceSquared[sector] = distanceSquared;
                        recordDepth[sector] = depthResult.Depth;
                    }
                }

                for (int sector = 0; sector < 4; sector++)
                {
                    var distanceSquared = recordDistanceSquared[sector];
                    if (distanceSquared >= 0 && distanceSquared < searchRadius * searchRadius)
                    {
                        var distance = Mathf.Sqrt(distanceSquared);
                        values[id, sector] = new DistanceDepth
                        {
                            Distance = distance,
                            Depth = recordDepth[sector]
                        };

                        pointCount[id]++;
                    }
                }
            }

            searchRadius *= 2;
        }

        return values;
    }

    private int CalculateSector(Vector2Int position, DepthPoint depthResult)
    {
        return (position.x > depthResult.Position.x ? 1 : 0) + (position.y > depthResult.Position.y ? 2 : 0);
    }

    private double[] CalculateEstimates(DistanceDepth[,] values)
    {
        int length = values.GetLength(0);
        double[] estimates = new double[length];

        for (int id = 0; id < length; id++)
        {
            double sumWeight = 0;
            double sumProduct = 0;

            for (int sector = 0; sector < 4; sector++)
            {
                var distance = values[id, sector].Distance;
                if (distance >= 0)
                {
                    double weight = 1 / (distance + 0.01);
                    sumWeight += weight;
                    sumProduct += weight * values[id, sector].Depth;
                }
            }

            estimates[id] = sumWeight > 0 ? sumProduct / sumWeight : 0;
        }

        return estimates;
    }
}
